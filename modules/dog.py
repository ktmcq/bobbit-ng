import random
import re

# Metadata
NAME    = 'dog'
ENABLE  = True
TYPE    = 'command'
PATTERN = re.compile('^!dog$')

USAGE   = '''Usage: !dog
Displays a random dog created by ASCII art
'''

# Command
DOGS = (
    "  __    __" \
        + "o-''))_____\\\\" \
        + "\"--__/ * * * )" \
        + "c_c__/-c____/",
    " ..^____/" \
        + "`-. ___ )" \
        + "  ||  || mh",
    "            __" \
        + "(\\,--------'()'--o" \
        + " (_    ___    /~\"" \
        + "  (_)_)  (_)_)",
    "           __" \
        + "      (___()'`;" \
        + "      /,    /`" \
        + "jgs   \\\\\"--\\\\",
    "^..^      /" \
        + "/_/\\_____/" \
        + "   /\\   /\\" \
        + "  /  \\ /  \\",
    "  __      _" \
        + "o'')}____//" \
        + " `_/      )" \
        + " (_(_/-(_/",
    ",'.-.'. " \
        + "'\\~ o/` ,," \
        + " { @ } f" \
        + " /`-'\\$" \
        + "(_/-\\_)",
    "   / \\__" \
        + "  (    @\\___" \
        + "  /         O" \
        + " /   (_____/" \
        + "/_____/   U",
    "  /^ ^\\" \
        + " / 0 0 \\" \
        + " V\\ Y /V" \
        + "  / - \\" \
        + " /    |" \
        + "V__) ||",
    "             .--~~,__" \
        + ":-....,-------`~~'._.'" \
        + " `-,,,  ,_      ;'~U'" \
        + "  _,-' ,'`-__; '--." \
        + " (_/'~~      ''''(;",
    "          __" \
        + " \\ ______/ V`-," \
        + "  }        /~~" \
        + " /_)^ --,r'" \
        + "|b      |b",
    "   __" \
    "o-''|\\_____/)" \
        + " \\_/|_)     )" \
        + "    \\  __  /" \
        + "    (_/ (_/",
    "  .-\"-." \
        + " /|6 6|\\" \
        + "{/(_0_)\\}" \
        + " _/ ^ \\_" \
        + "(/ /^\\ \\)-'" \
        + " \"\"' '\"\""
)

# Command
def command(bot, nick, message, channel, question=None):
    response = random.choice(DOGS)
    bot.send_response(response, nick, channel)

# Register
def register(bot):
    return (
        (PATTERN, command),
    )

# vim: set std=4 sw=4 ts=8 expandtab ft=python:
